# LeikirExo
Exercice proposé par l'agence web Leikir écrit en Angular (v2+)

Projet simple tirant parti des principales fonctionnalités du framework: web component, nouveau système de navigation, personalistation de composants, injection de service, de module partagés (SharedModule).

Respectant le guide style officiel d'Angular 

## Install 
1. Clone this repo
2. Simply run `ng serve` for a dev server. Then navigate to `http://localhost:4200/`.


## Possibilités / Améliorations
* La version actuelle de l'application utilise des appels à des services et un système de navigation basique pour charger et afficher les contacts. Mais elle pourrait aussi s'appuyer sur le système de state introduit par *rxjs/redux*
* Au lieu de faire deux appels au serveur pour récupérer les détails d'un contact tel fait actuellement, il est préférable de récupérer une fois la liste et faire passer les données déjà chargé entre les composants
* Créer un form input customisé pour upload les images
* Créer un composant pour l'édition des contacts car c'est meilleur de décomposer le plus pour réaliser des tests
* Un message d'avertissement lorsque l'utilisateur souhaite changer de contact lorsqu'il est en mode édition.

