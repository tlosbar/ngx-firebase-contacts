import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  // Lazy loading to improve the app performance
  { path: 'contacts', loadChildren: './contact/contact.module#ContactModule' }
];
