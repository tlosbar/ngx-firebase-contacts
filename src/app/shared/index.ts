import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CapitalizePipe } from './pipes/capitalize.pipe';

import { AcrylicCardComponent } from './components/acrylic-card/acrylic-card.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    // components
    AcrylicCardComponent,
    // pipes
    CapitalizePipe
  ],
  exports: [
    // components
    AcrylicCardComponent,
    // pipes
    CapitalizePipe,
    // modules
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule { }
