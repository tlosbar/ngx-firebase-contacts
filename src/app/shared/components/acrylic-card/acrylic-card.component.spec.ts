import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcrylicCardComponent } from './acrylic-card.component';

describe('AcrylicCardComponent', () => {
  let component: AcrylicCardComponent;
  let fixture: ComponentFixture<AcrylicCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcrylicCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcrylicCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
