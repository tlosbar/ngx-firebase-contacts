/**
 * Acrylic card component based on the new Windows UI style
 */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acrylic-card',
  templateUrl: './acrylic-card.component.html',
  styleUrls: ['./acrylic-card.component.scss']
})
export class AcrylicCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
