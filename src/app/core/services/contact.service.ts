import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Contact } from '../models/contact';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { FirebaseApp } from 'angularfire2';
import 'firebase/storage';

@Injectable()
export class ContactService {

  /**
   * Creates an instance of ContactService.
   *
   * @memberof ContactService
   */
  constructor(
    public firebaseApp: FirebaseApp,
    public database: AngularFireDatabase
  ) {
  }

  /**
   *
   *
   * @param {string} key of the contact to fetch
   * @returns {Observable<Contact>}
   *
   * @memberof ContactService
   */
  getContact(key: string): Observable<Contact> {
    return this.database.object(`/contacts/${key}`)
      .map(item => {
        const contact: Contact = {
          $key: item.$key,
          firstName: item.firstName,
          lastName: item.lastName,
          mobile: item.mobile,
          email: item.email,
          profilePicture: item.profilePicture,
          tags: item.tags || []
        };
        return contact;
      });
  }

  /**
   *
   *
   * @returns FirebaseListObservable<any[]>
   *
   * @memberof ContactService
   */
  getContacts(): FirebaseListObservable<any[]> {
    return this.database.list('/contacts', {
      query: {
        limitToLast: 15
      }
    });
  }

  getProfileImageUrl(contactKey) {
  }

  /**
   *
   *
   * @param {*} contact
   * @param {File} profileImageFile
   * @memberof ContactService
   */
  saveContact(contact: any, profileImageFile: File) {
    // Create a root reference
    const storageRef = this.firebaseApp.storage().ref();

    //  Save image in the firebase storage
    const profilePictureImgRef = storageRef.child(`images/${profileImageFile.name}`);

    profilePictureImgRef.put(profileImageFile).then((snapshot) => {
      profilePictureImgRef.getDownloadURL().then(url => {
        contact.profilePicture = url;
        this.database.list('/contacts').push(contact);
      });
    });
  }

  /**
   *
   *
   * @param {any} key of the contact to update
   * @param {any} values to update
   * @param {File} profileImageFile file to update
   * @memberof ContactService
   */
  updateContactAndProfilePicture(key, values, profileImageFile: File) {
    // Create a root reference
    const storageRef = this.firebaseApp.storage().ref();
    //  Save image in the firebase storage
    const profilePictureImgRef = storageRef.child(`images/${profileImageFile.name}`);
    profilePictureImgRef.put(profileImageFile).then((snapshot) => {
      profilePictureImgRef.getDownloadURL().then(url => {
        values.profilePicture = url;
        this.database.object(`/contacts/${key}`).set(values);
      });
    });
  }

  /**
   *
   *
   * @param {any} key of the contact to update
   *
   * @param {any} values to update
   * @memberof ContactService
   */
  updateContact(key, values) {
    this.database.object(`/contacts/${key}`).set(values);
  }

  /**
   *
   *
   * @param {any} key of the contact to update
   * @memberof ContactService
   */
  deleteContact(key) {
    this.database.object(`/contacts/${key}`).remove();
  }

}
