import { NgModule } from '@angular/core';
import { HttpModule, XHRBackend, RequestOptions, Http } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

// Services
import { ContactService } from './services/contact.service';

// Firebase config to connect to the database
export const firebaseConfig = {
  apiKey: 'AIzaSyBukF6M5ct1KRWdmlCAxcZKFPeHzoOcyrQ',
  authDomain: 'leikir-contact-list.firebaseapp.com',
  databaseURL: 'https://leikir-contact-list.firebaseio.com',
  projectId: 'leikir-contact-list',
  storageBucket: 'leikir-contact-list.appspot.com',
  messagingSenderId: '1072933730761'
};


@NgModule({
  declarations: [
  ],
  exports: [
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
  ],
  providers: [
    ContactService
  ]
})
export class CoreModule { }
