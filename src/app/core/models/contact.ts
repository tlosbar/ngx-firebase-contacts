/*
 * Contact model
 */

export class Contact {
  $key: string;
  firstName: string;
  lastName: string;
  email: string;
  mobile: string;
  profilePicture: string;
  tags: String[];
}
