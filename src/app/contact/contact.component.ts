import { Component, OnInit } from '@angular/core';
import { ContactService } from '../core/services/contact.service';
import { FirebaseListObservable } from 'angularfire2/database';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contacts$: FirebaseListObservable<any[]>;

  constructor(public contactService: ContactService) {
    // Fetch a firebaseListObservable of the contact. It will be updated automatically with changes
    this.contacts$ = this.contactService.getContacts();
  }

  ngOnInit() {
  }

}
