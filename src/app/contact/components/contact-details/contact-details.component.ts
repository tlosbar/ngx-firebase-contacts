import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import { ContactService } from '../../../core/services/contact.service';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Contact } from '../../../core/models/contact';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {
  contact: Contact;
  tagEntry = '';
  imgSrc = '';
  routerSubscription;

  editMode = false;
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService
  ) {
    // We get the slug of the selected contact thry router params and try to get the contact with service
    this.routerSubscription = this.route.params.subscribe(
      (params: any) => {
        const contactSlug = params['slug'];

        this.contactService.getContact(contactSlug).subscribe(contact => {
          this.contact = contact;
        });
      }
    );

    this.createForm();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }

  /**
   * Init the Reactive Form
   *
   * @memberof ContactDetailsComponent
   */
  createForm() {
    this.form = this.fb.group({
      'profilePicture': ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'mobile': ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern('\\d+')]],
      'email': ['', [Validators.required, Validators.email]],
      'tags': this.fb.array([])
    });
  }

  /**
   * Bind the data model to the form model
   *
   * @memberof ContactDetailsComponent
   */
  resetForm() {
    // Step 1: setting the main form group
    this.form.reset({
      'profilePicture': this.contact.profilePicture,
      'firstName': this.contact.firstName,
      'lastName': this.contact.lastName,
      'mobile': this.contact.mobile,
      'email': this.contact.email
    });

    // Step 2: setting the form array of tag
    this.setTags(this.contact.tags);

    // Step 3: setting the imgSrc
    this.imgSrc = this.contact.profilePicture;
  }

  /**
   * Set the FormArray of tags
   *
   * @param {any} tags
   * @memberof ContactDetailsComponent
   */
  setTags(tags) {
    const tagFormControls = tags.map(tag => this.fb.control(tag));
    const tagFormArray = this.fb.array(tagFormControls);
    this.form.setControl('tags', tagFormArray);
  }

  goToEditMode() {
    this.editMode = !this.editMode;
    this.resetForm();
  }

  /**
   * Handle when a new file is uploaded
   *
   * @param {any} event
   * @memberof ContactDetailsComponent
   */
  onFileUpload(event) {
    if (event.target.files && event.target.files[0]) {

      const file: File = event.target.files[0]; // get the file uploaded

      // We programmatically set the correct form and mark as dirty because of actual constraint of Angular Reactive forms
      this.form.get('profilePicture').setValue(file.name);
      this.form.get('profilePicture').markAsDirty();

      // Then we load the new image to the dom image
      const reader = new FileReader();
      reader.onloadend = () => {
        this.imgSrc = reader.result;
      };

      reader.readAsDataURL(file);
    }
  }

  /**
   * Add a new tag to the FormArray List
   *
   * @param {any} tag
   * @memberof ContactDetailsComponent
   */
  addTag(tag) {
    (<FormArray>this.form.get('tags')).push(new FormControl(tag));
    this.tagEntry = '';
  }

  /**
   * Cancel all the modification and quit the edit mode
   *
   * @memberof ContactDetailsComponent
   */
  revert() {
    this.editMode = false;
    this.resetForm();
  }

  /**
   * Update the contact and quit the edit mode
   *
   * @memberof ContactDetailsComponent
   */
  onSubmit() {
    // If the profile image was setted..
    if (this.form.get('profilePicture').dirty) {
      const file = (<HTMLInputElement>document.getElementById('file')).files[0];
      this.contactService.updateContactAndProfilePicture(this.contact.$key, this.form.value, file);
    } else {
      this.contactService.updateContact(this.contact.$key, this.form.value);
    }

    this.editMode = false;
  }

  /**
   * Remove the selected contact and close the details section
   *
   * @memberof ContactDetailsComponent
   */
  removeContact() {
    this.contactService.deleteContact(this.contact.$key);
    this.router.navigate(['/contacts']);
  }
}
