import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { FirebaseListObservable } from 'angularfire2/database';
import { ContactService } from '../../../core/services/contact.service';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {
  imgSrc = '/assets/images/addphoto.png';
  form: FormGroup;
  tagEntry = '';

  constructor(private fb: FormBuilder, private contactService: ContactService) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.form = this.fb.group({
      'profilePicture': ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'mobile': ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern('\\d+')]],
      'email': ['', [Validators.required, Validators.email]],
      'tags': this.fb.array([])
    });
  }

  /**
   * Handle when a new file is uploaded
   *
   * @param {any} event
   * @memberof AddContactComponent
   */
  onFileUpload(event) {
    if (event.target.files && event.target.files[0]) {

      const file: File = event.target.files[0]; // get the file uploaded

      // We programmatically set the correct form and mark as dirty because of actual constraint of Angular Reactive forms
      this.form.get('profilePicture').setValue(file.name);
      this.form.get('profilePicture').markAsDirty();

      const reader = new FileReader();
      reader.onloadend = () => {
        this.imgSrc = reader.result;
      };

      reader.readAsDataURL(file);
    }
  }

  /**
   * Add a new tag to the FormArray List
   *
   * @param {string} tag
   * @memberof AddContactComponent
   */
  addTag(tag: string) {
    (<FormArray>this.form.get('tags')).push(new FormControl(tag));
    this.tagEntry = '';
  }

  /**
   * Save the new contact
   *
   * @memberof AddContactComponent
   */
  onSubmit() {
    const file = (<HTMLInputElement>document.getElementById('file')).files[0];
    this.contactService.saveContact(this.form.value, file);
  }
}
