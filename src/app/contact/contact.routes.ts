import { Routes } from '@angular/router';
import { ContactComponent } from './contact.component';
import { AddContactComponent } from './components/add-contact/add-contact.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';

export const ContactRoutes: Routes = [
  {
    path: '', component: ContactComponent,  // Main contact component
    children: [
      { path: 'new', component: AddContactComponent },
      {
        path: ':slug',
        component: ContactDetailsComponent
      }
    ]
  }
];

