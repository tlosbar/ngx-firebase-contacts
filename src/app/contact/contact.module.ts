import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/index';

// Components
import { ContactComponent } from './contact.component';
import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { AddContactComponent } from './components/add-contact/add-contact.component';

// Routes
import { ContactRoutes as routes } from './contact.routes';

@NgModule({
  imports: [
    // Modules
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    // Components
    ContactDetailsComponent,
    ContactListComponent,
    ContactComponent,
    AddContactComponent
  ]
})
export class ContactModule { }
